<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Student;

require_once "../_bootstrap.inc.php";

try {

    $fields = [
        'id' => Request::getAsInteger('id'),
        'admission_number' => Request::getAsString('admission_number'),
        'admission_date' => Request::getAsString('admission_date'),
        'first_name' => Request::getAsString('first_name'),
        'last_name' => Request::getAsString('last_name'),
        'gender' => Request::getAsString('gender'),
        'date_of_birth' => Request::getAsString('date_of_birth'),
        'address' => Request::getAsString('address'),
        'contact_number' => Request::getAsString('contact_number'),
        'religion' => Request::getAsString('religion'),
        'nationality' => Request::getAsString('nationality'),
        'bc_number' => Request::getAsString('bc_number'),
        'bc_division' => Request::getAsString('bc_division'),
    ];


    $student = Student::build($fields);

    $result = $student->update();

    if ( $result ) {
        JSONResponse::validResponse('success');
        return;
    } else {
        throw new Exception('Failed to register student.');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
