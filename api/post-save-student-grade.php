<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\StudentGrade;

require_once "../_bootstrap.inc.php";


try {

    $fields = [
        'student_id' => Request::getAsInteger('student_id'),
        'grade_id' => Request::getAsInteger('grade_id'),
        'year' => Request::getAsInteger('year'),

    ];


    $studentGrade = StudentGrade::build($fields);


    $id = $studentGrade->insert();

    if ( $id != false ) {

        $s = StudentGrade::find($id);

        JSONResponse::validResponse(['student-grade' => $s]);
        return;

    } else {
        throw new Exception('Failed adding student to academic year');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}