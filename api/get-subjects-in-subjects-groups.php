<?php

use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Models\SubjectsInSubjectsGroup;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$results = SubjectsInSubjectsGroup::findAll();

JSONResponse::validResponse(['subjects_in_subjects_groups' => $results]);