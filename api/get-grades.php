<?php

use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Models\Grade;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$grades = Grade::findAll();

JSONResponse::validResponse(['grades' => $grades]);