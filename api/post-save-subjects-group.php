<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\SubjectsGroup;

require_once "../_bootstrap.inc.php";

try {

    $fields = [
        'subjects_group_name' => Request::getAsString('subjects_group_name')
    ];

    if ( empty($fields['subjects_group_name']) ) throw  new Exception('Subjects-group is empty');

    $subjectsGroup = SubjectsGroup::build($fields);

    // check if subject name already exist in the database
    if ( !empty(SubjectsGroup::findBySubjectsGroupName($subjectsGroup->subjects_group_name)) ) throw new Exception('Name already exist');

    $result = $subjectsGroup->insert();

    if ( empty($result) ) throw new Exception('Failed to insert subjects-group');

    $subject = SubjectsGroup::find($result);

    JSONResponse::validResponse(['subjects-group' => $subject]);
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
