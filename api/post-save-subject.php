<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Subject;

require_once "../_bootstrap.inc.php";

try {

    $fields = [
        'subject' => Request::getAsString('subject')
    ];

    if ( empty($fields['subject']) ) throw  new Exception('Subject is empty');

    $subject = Subject::build($fields);

    // check if subject name already exist in the database
    if ( !empty(Subject::findBySubject($subject->subject)) ) throw new Exception('Subject already exist');

    $result = $subject->insert();

    if ( empty($result) ) throw new Exception('Failed to insert subject');

    $subject = Subject::find($result);

    JSONResponse::validResponse(['subject' => $subject]);
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
