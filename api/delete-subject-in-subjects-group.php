<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\SubjectsInSubjectsGroup;

require_once "../_bootstrap.inc.php";

try {

    $fields = [
        'subject_id' => Request::getAsInteger('subject_id'),
        'subjects_group_id' => Request::getAsInteger('subjects_group_id'),
    ];


    $result = SubjectsInSubjectsGroup::deleteBySidAndSgid($fields['subject_id'], $fields['subjects_group_id']);

    if ( $result ) {
        JSONResponse::validResponse('Success');
        return;
    } else {
        throw new Exception('Failed');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
