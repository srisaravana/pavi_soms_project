<?php


use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Section;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

try {

    $fields = [
        'section_name' => Request::getAsString('section_name'),
        '_order' => Request::getAsInteger('_order'),
    ];


    $section = Section::build($fields);

    $id = $section->insert();

    if ( $id != false ) {

        $s = Section::find($id);

        JSONResponse::validResponse(['section' => $s]);
        return;

    } else {
        throw new Exception('Failed adding section');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
