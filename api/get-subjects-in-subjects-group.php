<?php

use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\SubjectsInSubjectsGroup;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$id = Request::getAsInteger('id');

$results = SubjectsInSubjectsGroup::getSubjectsBySubjectsGroup($id);

JSONResponse::validResponse(['subjects_in_subjects_group' => $results]);