<?php

use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Subject;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$query = Request::getAsInteger('id');

if ( empty($query) ) {
    JSONResponse::validResponse(['results' => null]);
    return;
}

$subject = Subject::find($query);
JSONResponse::validResponse(['subject' => $subject]);
return;
