<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Student;

require_once "../_bootstrap.inc.php";

$query = Request::getAsString('q');

if ( empty($query) ) {
    JSONResponse::validResponse(['results' => null]);
    return;
}

$students = Student::search($query);
JSONResponse::validResponse(['results' => $students]);
return;