<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\SubjectsInSubjectsGroup;

require_once "../_bootstrap.inc.php";

try {

    $fields = [
        'subject_id' => Request::getAsInteger('subject_id'),
        'subjects_group_id' => Request::getAsInteger('subjects_group_id')
    ];

    $object = SubjectsInSubjectsGroup::build($fields);

    $result = $object->insert();

    if ( empty($result) ) throw new Exception('Failed to insert.');

    $object = SubjectsInSubjectsGroup::find($result);

    JSONResponse::validResponse(['subject_in_subjects_group' => $object]);
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
