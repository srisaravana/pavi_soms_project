<?php

use App\Core\Requests\JSONResponse;
use App\Models\Subject;

require_once "../_bootstrap.inc.php";

$subjects = Subject::findAll();

JSONResponse::validResponse(['subjects' => $subjects]);