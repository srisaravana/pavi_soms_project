<?php

use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Student;

require_once "../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$query = Request::getAsInteger('id');

if ( empty($query) ) {
    JSONResponse::validResponse(['results' => null]);
    return;
}

$student = Student::find($query);
JSONResponse::validResponse(['student' => $student]);
return;
