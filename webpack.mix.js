const mix = require('laravel-mix');

mix.options({
    terser: {
        extractComments: false,
    }
});

mix.js('vue/student-grade/add-student-grade.js', 'soms/student-grade/add-student-grade.min.js');
mix.js('vue/students/register-student.js', 'soms/students/register-student.min.js');
mix.js('vue/students/edit-student.js', 'soms/students/edit-student.min.js');
mix.js('vue/subjects/manage-subjects.js', 'soms/subjects/manage-subjects.min.js');

mix.js('vue/classes/sections/manage-sections.js', 'soms/classes/sections/manage-sections.min.js');
mix.js('vue/subjects-groups/manage-subjects-groups.js', 'soms/subjects-groups/manage-subjects-groups.min.js');