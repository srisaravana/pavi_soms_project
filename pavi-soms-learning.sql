-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5991
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pavi_soms_learning
DROP DATABASE IF EXISTS `pavi_soms_learning`;
CREATE DATABASE IF NOT EXISTS `pavi_soms_learning` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `pavi_soms_learning`;

-- Dumping structure for table pavi_soms_learning.academic_staffs
DROP TABLE IF EXISTS `academic_staffs`;
CREATE TABLE IF NOT EXISTS `academic_staffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` enum('MR','MRS','MS') NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `tempAddress` varchar(255) DEFAULT NULL,
  `permAddress` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `maritalStatus` enum('MARRIED','SINGLE','DIVORCED','SEPARATED') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table pavi_soms_learning.academic_staffs: ~8 rows (approximately)
/*!40000 ALTER TABLE `academic_staffs` DISABLE KEYS */;
REPLACE INTO `academic_staffs` (`id`, `title`, `fullName`, `tempAddress`, `permAddress`, `email`, `gender`, `religion`, `maritalStatus`) VALUES
	(1, 'MR', 'David banana', 'somehwere', '', 'david@gmail.com', 'M', 'Hindu', 'SINGLE'),
	(2, 'MS', 'Kamalini', 'some where', NULL, NULL, 'F', NULL, 'DIVORCED'),
	(3, 'MR', 'Birlaa', 'somewhere\\nhello.', 'somewhere\\nhello.', NULL, 'M', NULL, 'MARRIED'),
	(4, 'MR', 'Kumar Balamurali', '', 'some place, somewhere', 'sdfsdf', 'M', '', 'SINGLE'),
	(7, 'MRS', 'David Birla', '#23, Main street,\r\nValaichenai', '#56/23,\r\nBar road, Batticaloa', 'david.b@gmail.com', 'M', '', 'SINGLE'),
	(9, 'MR', 'Banana Mango', 'Addr1', 'Addr2', 'banana.mango@gmail.com', 'F', 'Flying Spegetti Monster', 'SEPARATED'),
	(11, 'MRS', 'Srisaravana', '', '', 'test2@gmail.com', 'F', '', 'SINGLE'),
	(12, 'MR', 'Hello World!', 'test 2', '', 'test2@gmail.com', 'M', '', 'MARRIED'),
	(13, 'MRS', 'Apple Mango', 'some address', 'some other address\nanother line of text\nmain road.', 'apple@mango.com', 'F', '', 'SINGLE');
/*!40000 ALTER TABLE `academic_staffs` ENABLE KEYS */;

-- Dumping structure for table pavi_soms_learning.grades
DROP TABLE IF EXISTS `grades`;
CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(255) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_classes_sections` (`section_id`),
  CONSTRAINT `FK_classes_sections` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table pavi_soms_learning.grades: ~0 rows (approximately)
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
REPLACE INTO `grades` (`id`, `grade_name`, `section_id`) VALUES
	(1, 'Grade 1', 1),
	(2, 'Grade 11', 7);
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;

-- Dumping structure for table pavi_soms_learning.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table pavi_soms_learning.sections: ~2 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
REPLACE INTO `sections` (`id`, `section_name`) VALUES
	(1, 'Primary'),
	(6, 'Secondary'),
	(7, 'Upper');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table pavi_soms_learning.students
DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admission_no` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `birth_ds` varchar(100) DEFAULT NULL,
  `nationality` varchar(100) DEFAULT NULL,
  `bc_no` int(11) DEFAULT NULL,
  `blood_group` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pavi_soms_learning.students: ~0 rows (approximately)
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- Dumping structure for table pavi_soms_learning.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` enum('ADMIN','MANAGER','USER') DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table pavi_soms_learning.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `display_name`, `created_at`, `updated_at`, `role`) VALUES
	(1, 'admin', '$2y$10$URgtJ0mB3MJVN3He6ikx2OTlvucm9KYszClldBjPcVYBMhJ7LPzUW', 'Admin', '2020-06-08 14:26:21', '2020-06-16 14:37:49', 'ADMIN'),
	(2, 'user', '$2y$10$URgtJ0mB3MJVN3He6ikx2OTlvucm9KYszClldBjPcVYBMhJ7LPzUW', 'Normal User', '2020-06-08 14:26:21', '2020-06-18 20:36:54', 'USER');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
