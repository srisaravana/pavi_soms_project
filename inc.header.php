<?php

use App\Core\App;
use App\Core\Authentication;
use App\Models\User;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= App::getTitle() ?></title>
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/app.css">
</head>
<body>


<?php if ( Authentication::isAuthenticated() || Authentication::isAuthenticated(User::ROLE_ADMIN) || Authentication::isAuthenticated(User::ROLE_MANAGER) ): ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
        <a class="navbar-brand" href="<?= App::siteURL() ?>">SOMS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= App::siteURL() ?>">Home</a>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Academic Staff
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= App::url('/soms/academic-staff/manage.php') ?>">View academic staff</a>
                        <a class="dropdown-item" href="<?= App::url('/soms/academic-staff/add.php') ?>">Add a new academic staff</a>

                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Classes
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= App::url('/soms/classes/sections/manage.php') ?>">Manage sections</a>
                        <a class="dropdown-item" href="<?= App::url('/soms/classes/grades/manage.php') ?>">Manage grades</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Subjects
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= App::url('/soms/subjects/manage.php') ?>">Manage subjects</a>
                        <a class="dropdown-item" href="<?= App::url('/soms/subjects-groups/manage.php') ?>">Manage subject groups</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Students
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= App::url('/soms/students/register-student.php') ?>">Register a new student</a>

                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Academic Year
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= App::url('/soms/student-grade/add.php') ?>">Assign a student to year</a>

                    </div>
                </li>

            </ul>

            <div>
                <a href="<?= App::url('/logout.php') ?>" class="btn btn-danger">Logout</a>
            </div>

        </div>


    </nav>

<?php endif; ?>

