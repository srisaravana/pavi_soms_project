<?php

use App\Core\App;
use App\Core\Authentication;
use App\Core\Requests\Request;

include_once "../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

App::setTitle("Edit Student");

$id = Request::getAsInteger('id');
?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<input type="hidden" id="field-student-id" value="<?= $id ?>">

<div id="app"></div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="edit-student.min.js"></script>
