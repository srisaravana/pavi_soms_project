<?php

use App\Core\App;
use App\Core\Authentication;

include_once "../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();
App::setTitle("Assign Student:Grade");
?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div id="app"></div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="add-student-grade.min.js"></script>

