<?php

use App\Core\App;
use App\Core\Authentication;
use App\Core\Requests\Request;
use App\FormHelper;
use App\Models\Grade;
use App\Models\Section;

include_once "../../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$gradeToEdit = Grade::find(Request::getAsInteger('id'));
$sectionsDropdown = Section::getAllForDropdown();

if ( empty($gradeToEdit) ) die('Invalid grade');

App::setTitle("Edit section - " . $gradeToEdit->grade_name);

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">

    <div class="row">
        <div class="col">
            <h4 class="text-center text-uppercase">Edit grade</h4>
            <hr>
        </div>
    </div>


    <div class="row justify-content-center">

        <div class="col-8">

            <div class="card">
                <div class="card-header">Edit grade</div>
                <div class="card-body">

                    <form id="form_edit_grade">

                        <input type="hidden" id="field_grade_id" value="<?= $gradeToEdit->id ?>">

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="field_grade_name">Grade name</label>
                                    <input type="text" id="field_grade_name" class="form-control" value="<?= $gradeToEdit->grade_name ?>">
                                    <div class="invalid-feedback">Grade name is required</div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="field_section_id">Section</label>
                                    <select id="field_section_id" class="form-control">
                                        <?php FormHelper::createSelectOptions($sectionsDropdown, $gradeToEdit->section_id); ?>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="text-right">
                            <button class="btn btn-primary" id="btn_update_grade">Update</button>
                        </div>

                    </form>


                </div>
            </div>

        </div>


        <div class="col-8">

        </div>
    </div>
</div>


<!-- Modal: Delete Confirmation -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure do you want to delete the following grade?</p>
                <div class="border rounded-sm text-center p-2">
                    <div class="lead"><?= $gradeToEdit->section_name ?></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?= App::url('/soms/classes/sections/delete.php', ['id' => $gradeToEdit->id]) ?>" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="edit.js"></script>