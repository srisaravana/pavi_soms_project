<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Grade;

require_once "../../../_bootstrap.inc.php";

try {

    $fields = [
        'grade_name' => Request::getAsString('grade_name'),
        'section_id' => Request::getAsString('section_id')
    ];

    if ( !is_null(Grade::nameExists($fields['grade_name'])) ) {
        throw new Exception('Grade already exists');
    }

    $grade = Grade::build($fields);

    $result = $grade->insert();

    if ( !is_null($result) ) {
        $grade = Grade::find($result);
        if ( !is_null($grade) ) {
            JSONResponse::validResponse(['grade' => $grade]);
            return;
        }
    } else {
        throw new Exception('Failed to save grade.');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}