<?php

use App\Core\App;
use App\Core\Authentication;
use App\Core\Requests\Request;
use App\FormHelper;
use App\Models\Grade;
use App\Models\Section;

include_once "../../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

App::setTitle("Manage class grades");

$sectionsDropdown = Section::getAllForDropdown();
$sections = Section::findAll();

$error = Request::getAsString('error');
$message = Request::getAsString('message');

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">

    <div class="row">
        <div class="col">
            <h4 class="text-center text-uppercase">Manage class grades</h4>
            <hr>
            <?php if ( !is_null($error) ): ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <span><?= $error ?></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if ( !is_null($message) ): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <span><?= $message ?></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    </div>


    <div class="row">

        <div class="col-4">

            <div class="card">
                <div class="card-header">Add a grade</div>
                <div class="card-body">

                    <form>

                        <div class="form-group">
                            <label for="field_grade_name">Grade name</label>
                            <input type="text" id="field_grade_name" class="form-control">
                            <div class="invalid-feedback">Grade name is required</div>
                        </div>

                        <div class="form-group">
                            <label for="field_section_id">Section</label>
                            <select id="field_section_id" class="form-control">
                                <?php FormHelper::createSelectOptions($sectionsDropdown); ?>
                            </select>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" id="btn_save_grade" type="button">Save</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>

        <div class="col-8">

            <div class="border rounded-sm p-3">
                <?php foreach ( $sections as $section ): ?>
                    <div class="mb-3">
                        <div class="mb-2 font-weight-bold"><a href="<?= App::url('/soms/classes/sections/edit.php', ['id' => $section->id]) ?>"><?= $section->section_name ?></a></div>
                        <?php $grades = $section->getAllGrades(); ?>
                        <?php if ( !is_null($grades) ): ?>
                            <ul class="list-group ml-2">
                                <?php foreach ( $grades as $grade ): ?>
                                    <li class="list-group-item p-1"><a href="<?= App::url('/soms/classes/grades/edit.php', ['id' => $grade->id]) ?>"><?= $grade->grade_name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            <div>No grades in <?= $section->section_name ?>.</div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="add.js"></script>
