$(function () {

    $("#btn_save_grade").on("click", function () {
        console.log("saving grade...");



        let fields = {
            'grade_name': $("#field_grade_name"),
            'section_id': $("#field_section_id")
        };

        let validated = true;

        if (isEmptyField(fields.grade_name)) {
            validated = false;
        }

        if (!validated) return false;

        $.post(`${getSiteURL()}/soms/classes/grades/_process_add.php`, {
            'grade_name': getStringValue(fields.grade_name),
            'section_id': getStringValue(fields.section_id)
        }).done(function (response) {

            reload();

        }).fail(function (error) {
            alert(error.responseJSON.message);
        });

    });

});
