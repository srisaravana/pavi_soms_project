<?php


use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Grade;

require_once "../../../_bootstrap.inc.php";


Authentication::isAdminOrRedirect();

try {

    $fields = [
        'id' => Request::getAsInteger('id'),
        'grade_name' => Request::getAsString('grade_name'),
        'section_id' => Request::getAsInteger('section_id'),
    ];


    $grade = Grade::find($fields['id']);

    if ( empty($grade) ) throw new Exception('Invalid section');

    $grade->grade_name = $fields['grade_name'];
    $grade->section_id = $fields['section_id'];

    if ( $grade->update() ) {
        JSONResponse::validResponse('Updated');

    } else {
        throw new Exception('Update failed');
    }

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}