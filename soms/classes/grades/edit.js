$(function () {

    $("#form_edit_grade").on("submit", function (event) {
        event.preventDefault();

        let fieldGradeId = $("#field_grade_id");
        let fieldGradeName = $("#field_grade_name");
        let fieldSectionId = $("#field_section_id");

        let validated = true;

        if (isEmptyField(fieldGradeName)) {
            validated = false;
        }

        if (!validated) return false;

        $.post(`${getSiteURL()}/soms/classes/grades/_process_edit.php`, {
            'id': getIntegerValue(fieldGradeId),
            'grade_name': getStringValue(fieldGradeName),
            'section_id': getIntegerValue(fieldSectionId),
        }).done(function (response) {

            redirect(`${getSiteURL()}/soms/classes/grades/manage.php?message=${getStringValue(fieldGradeName)} updated successfully.`)

        }).fail(function (error) {
            alert(error.responseJSON.message);
        });

    });

});