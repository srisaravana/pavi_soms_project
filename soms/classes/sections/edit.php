<?php

use App\Core\App;
use App\Core\Authentication;
use App\Core\Requests\Request;
use App\Models\Section;

include_once "../../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

$sectionToEdit = Section::find(Request::getAsInteger('id'));

if ( empty($sectionToEdit) ) die('Invalid section');

App::setTitle("Edit section - " . $sectionToEdit->section_name);

$grades = $sectionToEdit->getAllGrades();

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">

    <div class="row">
        <div class="col">
            <h4 class="text-center text-uppercase">Edit section</h4>
            <hr>
        </div>
    </div>


    <div class="row">

        <div class="col-4">

            <div class="card">
                <div class="card-header">Edit section</div>
                <div class="card-body">

                    <form id="form_edit_section">

                        <input type="hidden" id="field_section_id" value="<?= $sectionToEdit->id ?>">

                        <div class="form-group">
                            <label for="field_section_name"></label>
                            <input type="text" id="field_section_name" class="form-control" value="<?= $sectionToEdit->section_name ?>">
                            <div class="invalid-feedback">Section name is required</div>
                        </div>

                        <div class="form-group">
                            <label for="field_order">Order value</label>
                            <input type="number" id="field_order" class="form-control" value="<?= $sectionToEdit->_order ?>">
                        </div>

                        <div class="text-right">
                            <button class="btn btn-primary" id="btn_save_section">Update</button>
                        </div>

                    </form>

                    <?php if ( !is_null($grades) ): ?>
                        <hr>
                        <div class="mb-2 font-weight-bold">Grades in <?= $sectionToEdit->section_name ?>.</div>
                        <ul class="list-group">
                            <?php foreach ( $grades as $grade ): ?>
                                <li class="list-group-item p-1"><?= $grade->grade_name ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <div>No grades in <?= $sectionToEdit->section_name ?>.</div>
                        <div>You can <a class="text-danger" href="#" data-toggle="modal" data-target="#deleteModal">delete</a> this section.</div>
                    <?php endif; ?>

                </div>
            </div>

        </div>


        <div class="col-8">
            <?php include_once "_parts/section_table.php"; ?>
        </div>
    </div>
</div>


<!-- Modal: Delete Confirmation -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure do you want to delete the following section?</p>
                <div class="border rounded-sm text-center p-2">
                    <div class="lead"><?= $sectionToEdit->section_name ?></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?= App::url('/soms/classes/sections/delete.php', ['id' => $sectionToEdit->id]) ?>" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="edit.js"></script>