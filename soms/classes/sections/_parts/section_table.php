<?php use App\Core\App;
use App\Models\Section;

$sections = Section::findAll();
?>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Section name</th>
        <th>Order</th>
    </tr>
    </thead>

    <tbody>

    <?php foreach ( $sections as $section ): ?>
        <tr>
            <td><a href="<?= App::url('/soms/classes/sections/edit.php', ['id' => $section->id]) ?>"><?= $section->section_name ?></a></td>
            <td><?= $section->_order ?></td>
        </tr>
    <?php endforeach; ?>

    </tbody>

</table>