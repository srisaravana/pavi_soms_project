<?php


use App\Core\Authentication;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\Section;

require_once "../../../_bootstrap.inc.php";


Authentication::isAdminOrRedirect();

try {

    $fields = [
        'id' => Request::getAsInteger('id'),
        'section_name' => Request::getAsString('section_name'),
        '_order' => Request::getAsInteger('_order'),
    ];


    $section = Section::find($fields['id']);

    if ( empty($section) ) throw new Exception('Invalid section');

    $section->section_name = $fields['section_name'];
    $section->_order = $fields['_order'];

    if ( $section->update() ) {
        JSONResponse::validResponse('Updated');

    } else {
        throw new Exception('Update failed');
    }

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}