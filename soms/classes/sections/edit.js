$(function () {

    $("#form_edit_section").on("submit", function (event) {
        event.preventDefault();

        let fieldSectionName = $("#field_section_name");
        let fieldSectionId = $("#field_section_id");
        let fieldOrder = $("#field_order");

        let validated = true;

        if (isEmptyField(fieldSectionName)) {
            validated = false;
        }


        if (!validated) return false;


        $.post(`${getSiteURL()}/soms/classes/sections/_process_edit.php`, {
            'id': getIntegerValue(fieldSectionId),
            'section_name': getStringValue(fieldSectionName),
            '_order': getIntegerValue(fieldOrder),
        }).done(function (response) {

            reload();

        }).fail(function (error) {
            alert(error.responseJSON.message);
        });

    });

});