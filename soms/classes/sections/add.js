$(function () {

    $("#form_add_section").on("submit", function (event) {

        event.preventDefault();
        let fieldSectionName = $("#field_section_name");
        let fieldOrder = $("#field_order");


        let validated = true;

        if (isEmptyField(fieldSectionName)) {
            validated = false;
        }


        if (!validated) return false;


        $.post(`${getSiteURL()}/soms/classes/sections/_process_add.php`, {
            'section_name': getStringValue(fieldSectionName),
            '_order': getIntegerValue(fieldOrder)
        }).done(function (response) {

            reload();

        }).fail(function (error) {
            console.log(error.responseJSON.message);
            alert("Failed to save section");
        });
    });

});