<?php

use App\Core\App;
use App\Core\Requests\Request;
use App\Models\Section;

require_once "../../../_bootstrap.inc.php";

$sectionId = Request::getAsInteger('id');

try {

    if ( is_null($sectionId) ) throw new Exception('Delete Section: Section id is required.');

    $section = Section::find($sectionId);

    if ( is_null($section) ) throw new Exception('Delete Section: Invalid section.');


    if ( $section->delete() ) {
        App::redirect('/soms/classes/sections/manage.php', ['message' => sprintf('Section \'%s\' deleted successfully.', $section->section_name)]);
    } else {
        throw new Exception(sprintf('Error deleting \'%s\'', $section->section_name));
    }


} catch ( Exception $exception ) {
    App::redirect('/soms/classes/sections/manage.php', ['error' => $exception->getMessage()]);
}