$(function () {

    $("#btn_add_staff").on("click", function () {

        let fields = {
            "title": $("#field_title"),
            "fullName": $("#field_full_name"),
            "tempAddress": $("#field_temp_address"),
            "permAddress": $("#field_perm_address"),
            "email": $("#field_email"),
            "gender": $("#field_gender"),
            "religion": $("#field_religion"),
            "maritalStatus": $("#field_marital_status"),
        };

        let validated = true;

        /**
         * validating fields
         */
        if (isEmptyField(fields.fullName)) {
            validated = false;
        }

        if (isEmptyField(fields.email)) {
            validated = false;
        }

        /**
         * if all fields are validated,
         * proceed to send ajax request!
         */

        if (!validated) return false;

        $.post(`${getSiteURL()}/soms/academic-staff/_process_add.php`, {
            title: getStringValue(fields.title),
            fullName: getStringValue(fields.fullName),
            tempAddress: getStringValue(fields.tempAddress),
            permAddress: getStringValue(fields.permAddress),
            email: getStringValue(fields.email),
            gender: getStringValue(fields.gender),
            religion: getStringValue(fields.religion),
            maritalStatus: getStringValue(fields.maritalStatus),

        }).done(function (response) {

            let staff = response.staff;

            redirect(`${getSiteURL()}/soms/academic-staff/edit.php?id=${staff.id}`);

        }).fail(function (response) {

            alert("something went wrong!");
            console.log(response);

        });

    });

});

