<?php

use App\Core\App;
use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\AcademicStaff;

require_once "../../_bootstrap.inc.php";

$fields = [
    'id' => Request::getAsInteger('id'),
    'title' => Request::getAsString('title'),
    'fullName' => Request::getAsString('fullName'),
    'tempAddress' => Request::getAsString('tempAddress'),
    'permAddress' => Request::getAsString('permAddress'),
    'email' => Request::getAsString('email'),
    'gender' => Request::getAsString('gender'),
    'maritalStatus' => Request::getAsString('maritalStatus'),
    'religion' => Request::getAsString('religion'),
];


try {

    if ( is_null($fields['id']) ) {
        throw new Exception("Invalid ID");
    }

    $staff = AcademicStaff::find($fields['id']);

    if ( empty($staff) ) {
       throw new Exception("Invalid staff");
    }

    $staff = AcademicStaff::build($fields);

    if ( $staff->update() ) {
        JSONResponse::validResponse('Staff updated');
        return;
    } else {
        JSONResponse::invalidResponse('Update failed!');
        return;
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}