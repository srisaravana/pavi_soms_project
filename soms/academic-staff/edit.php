<?php

use App\Core\App;
use App\Core\Authentication;
use App\Core\Requests\Request;
use App\FormHelper;
use App\Models\AcademicStaff;

include_once "../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

App::setTitle("View Academic Staff");

$id = Request::getAsInteger('id');

// if id is null, which means it wasn't passed on url
if ( is_null($id) ) App::redirect('manage.php');

$staff = AcademicStaff::find($id);

if ( is_null($staff) ) App::redirect('manage.php');

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">
    <div class="row">
        <div class="col">


            <div class="card">
                <div class="card-header">Edit - <span id="card_header_full_name"><?= $staff->fullName ?></span></div>
                <div class="card-body">


                    <input type="hidden" id="field_id" value="<?= $staff->id ?>">

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="">Title</label>
                                <select name="title" id="field_title" class="form-control">
                                    <?php FormHelper::createSelectOptions(AcademicStaff::TITLE, $staff->title); ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-8">
                            <div class="form-group">
                                <label for="">Full name</label>
                                <input type="text" class="form-control" id="field_full_name" value="<?= $staff->fullName ?>">
                                <div class="invalid-feedback">Full name is required</div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Temp Address</label>
                                <textarea name="tempAddress" class="form-control" id="field_temp_address" rows="5"><?= $staff->tempAddress ?></textarea>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">Perm Address</label>
                                <textarea name="permAddress" class="form-control" id="field_perm_address" rows="5"><?= $staff->permAddress ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col">

                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" id="field_email" value="<?= $staff->email ?>">
                                <div class="invalid-feedback">Email is required.</div>
                                <div class="valid-feedback">Email looks good!</div>
                            </div>

                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">Gender</label>
                                <select name="gender" id="field_gender" class="form-control">
                                    <?php FormHelper::createSelectOptions(AcademicStaff::GENDER, $staff->gender); ?>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">Religion</label>
                                <input type="text" class="form-control" id="field_religion" value="<?= $staff->religion ?>">
                            </div>
                        </div>

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-4">

                            <div class="form-group">
                                <label for="">Marital status</label>
                                <select name="maritalStatus" id="field_marital_status" class="form-control">
                                    <?php FormHelper::createSelectOptions(AcademicStaff::MARITAL_STATUS, $staff->maritalStatus); ?>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="text-right">
                        <button type="button" id="btn_update_staff" class="btn btn-primary">Update</button>
                    </div>

                </div>


            </div>


        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="edit.js"></script>