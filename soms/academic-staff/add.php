<?php

use App\Core\App;
use App\Core\Authentication;
use App\FormHelper;
use App\Models\AcademicStaff;

include_once "../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

App::setTitle("Add a new staff");

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">
    <div class="row">
        <div class="col">


            <div class="card">
                <div class="card-header">Add a new academic staff</div>
                <div class="card-body">

                    <form>


                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <select name="title" id="field_title" class="form-control">
                                        <?php FormHelper::createSelectOptions(AcademicStaff::TITLE); ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-8">
                                <div class="form-group">
                                    <label for="">Full name</label>
                                    <input type="text" class="form-control" id="field_full_name">
                                    <div class="invalid-feedback">Full name is required.</div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Temp Address</label>
                                    <textarea name="tempAddress" class="form-control" id="field_temp_address" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Perm Address</label>
                                    <textarea name="permAddress" class="form-control" id="field_perm_address" rows="5"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col">

                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" id="field_email">
                                </div>

                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Gender</label>
                                    <select name="gender" id="field_gender" class="form-control">
                                        <?php FormHelper::createSelectOptions(AcademicStaff::GENDER); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Religion</label>
                                    <input type="text" class="form-control" id="field_religion">
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center">
                            <div class="col-4">

                                <div class="form-group">
                                    <label for="">Marital status</label>
                                    <select id="field_marital_status" class="form-control">
                                        <?php FormHelper::createSelectOptions(AcademicStaff::MARITAL_STATUS); ?>
                                    </select>
                                </div>

                            </div>
                        </div>



                        <div class="text-right">
                            <button type="button" class="btn btn-primary" id="btn_add_staff">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>

                    </form>

                </div>

            </div>


        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

<script src="add.js"></script>
