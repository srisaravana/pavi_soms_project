<?php

use App\Core\Requests\JSONResponse;
use App\Core\Requests\Request;
use App\Models\AcademicStaff;

require_once "../../_bootstrap.inc.php";


try {

    $fields = [
        'title' => Request::getAsString('title'),
        'fullName' => Request::getAsString('fullName'),
        'tempAddress' => Request::getAsString('tempAddress'),
        'permAddress' => Request::getAsString('permAddress'),
        'email' => Request::getAsString('email'),
        'gender' => Request::getAsString('gender'),
        'maritalStatus' => Request::getAsString('maritalStatus'),
    ];


    $staff = AcademicStaff::build($fields);


    $id = $staff->insert();

    if ( $id != false ) {

        $s = AcademicStaff::find($id);

        JSONResponse::validResponse(['staff' => $s]);
        return;

    } else {
        throw new Exception('Failed adding academic staff');
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}