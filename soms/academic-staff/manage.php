<?php

use App\Core\App;
use App\Core\Authentication;
use App\Models\AcademicStaff;

include_once "../../_bootstrap.inc.php";

Authentication::isAdminOrRedirect();

App::setTitle("Manage academic staff");


$staffs = AcademicStaff::findAll();

?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">
    <div class="row">
        <div class="col">

            <h3>Manage Academic Staff</h3>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Full name</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Religion</th>
                    <th>Marital Status</th>
                </tr>
                </thead>

                <tbody>

                <?php foreach ( $staffs as $staff ): ?>
                    <tr>
                        <td><a href="<?= App::url('/soms/academic-staff/edit.php', ['id' => $staff->id]) ?>"><?= $staff->fullName ?></a></td>
                        <td><?= $staff->getGender() ?></td>
                        <td><?= $staff->email ?></td>
                        <td><?= $staff->religion ?></td>
                        <td><?= $staff->getMaritalStatus() ?></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>

            </table>

        </div>
    </div>
</div>

<?php include_once BASE_PATH . "/inc.footer.php"; ?>

