import ManageSubjectsGroups from "./manage-subjects-groups/ManageSubjectsGroups";

import Vue from 'vue';

new Vue({
    render: h => h(ManageSubjectsGroups)
}).$mount('#app');