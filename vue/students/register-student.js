import RegisterStudent from "./register-student/RegisterStudent";
import Vue from 'vue';

new Vue({
    render: h => h(RegisterStudent)
}).$mount('#app');