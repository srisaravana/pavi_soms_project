import EditStudent from "./edit-student/EditStudent";
import Vue from 'vue';

new Vue({
    render: h => h(EditStudent)
}).$mount('#app');