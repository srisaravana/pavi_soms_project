<?php


namespace App\Models;


use App\Core\Database\Database;

class User implements IModel
{

    public int $id;
    public string $username, $display_name, $password, $role;

    const ROLES = [
        'ADMIN' => 'Administrator',
        'USER' => 'User',
        'MANAGER' => 'Manager',
    ];

    const ROLE_ADMIN = 'ADMIN';
    const ROLE_USER = 'USER';
    const ROLE_MANAGER = 'MANAGER';


    /**
     * Find a user by id
     * @param int $id
     * @return User|null
     */
    public static function find(int $id)
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where id=? limit 1");
        $statement->execute([$id]);

        $result = $statement->fetchObject(self::class);

        if ( !empty($result) ) return $result;
        return null;

    }


    public static function findAll($limit = 1000, $offset = 0)
    {
        // TODO: Implement findAll() method.
    }

    public function insert()
    {
        // TODO: Implement insert() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public static function findByUsername(string $username)
    {
        $db = Database::instance();
        $statement = $db->prepare("select * from users where username=? limit 1");
        $statement->execute([$username]);

        $result = $statement->fetchObject(self::class);

        if ( !empty($result) ) return $result;
        return null;
    }


}