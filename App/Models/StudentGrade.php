<?php


namespace App\Models;


use App\Core\Database\Database;

class StudentGrade implements IModel
{

    private const _TABLE = 'student_grade';

    public ?int $id, $student_id, $grade_id;
    public ?string $year;

    public static function build($array)
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return StudentGrade|null
     */
    public static function find(int $id)
    {
        return Database::find(self::_TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return StudentGrade[]
     */
    public static function findAll($limit = 100, $offset = 0)
    {
        return Database::findAll(self::_TABLE, $limit, $offset, self::class, 'year');
    }

    public function insert()
    {
        $data = [
            'student_id' => $this->student_id,
            'grade_id' => $this->grade_id,
            'year' => $this->year,
        ];

        return Database::insert(self::_TABLE, $data);

    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}