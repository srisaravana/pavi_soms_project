<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class SubjectsInSubjectsGroup implements IModel
{

    private const TABLE = 'subjects_in_subjects_groups';


    public ?int $id, $subject_id, $subjects_group_id;

    /**
     * @param $array
     * @return SubjectsInSubjectsGroup
     */
    public static function build($array): SubjectsInSubjectsGroup
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return SubjectsInSubjectsGroup|null
     */
    public static function find(int $id): ?SubjectsInSubjectsGroup
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return SubjectsInSubjectsGroup[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class, 'subjects_group_id');
    }


    /**
     * @return bool|int|null
     */
    public function insert()
    {
        $data = [
            'subject_id' => $this->subject_id,
            'subjects_group_id' => $this->subjects_group_id
        ];

        return Database::insert(self::TABLE, $data);

    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            'subject_id' => $this->subject_id,
            'subjects_group_id' => $this->subjects_group_id
        ];

        return Database::update(self::TABLE, $data, ['id' => $this->id]);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete(self::TABLE, 'id', $this->id);
    }


    /**
     * @param $subjects_group_id
     * @return Subject[]|null
     */
    public static function getSubjectsBySubjectsGroup($subjects_group_id): ?array
    {
        $db = Database::instance();
        $statement = $db->prepare("SELECT * FROM " . self::TABLE . " WHERE subjects_group_id = ?");
        $statement->execute([$subjects_group_id]);

        /** @var SubjectsInSubjectsGroup[] $results */
        $results = $statement->fetchAll(PDO::FETCH_CLASS, self::class);

        if ( !empty($results) ) {

            $subjects = [];
            foreach ( $results as $r ) {
                $subjects[] = Subject::find($r->subject_id);
            }

            if ( !empty($subjects) ) return $subjects;
            else return null;
        }
        return null;

    }

    /**
     * @param $subject_id
     * @param $subjects_group_id
     * @return bool
     */
    public static function deleteBySidAndSgid($subject_id, $subjects_group_id): bool
    {
        $db = Database::instance();
        $statement = $db->prepare("DELETE FROM " . self::TABLE . ' WHERE subject_id=? AND subjects_group_id=?');
        return $statement->execute([$subject_id, $subjects_group_id]);
    }

}