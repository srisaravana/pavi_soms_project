<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class Student implements IModel
{

    private const _TABLE = 'students';

    public const GENDER = [
        'M' => 'Male',
        'F' => 'Female'
    ];

    public const NATIONALITY = [
        'SRILANKAN' => 'Sri Lankan',
        'NONSRILANKAN' => 'Non Sri Lankan',
    ];

    public const RELIGION = [
        'HINDUISM' => 'Hinduism',
        'CHRISTIANITY' => 'Christianity',
        'CATHOLIC' => 'Catholic',
        'ISLAM' => 'Islam',
        'BUDDHISM' => 'Buddhism',
        'OTHER' => 'Other',
    ];


    public ?int $id;
    public ?string $admission_number, $admission_date, $first_name, $last_name, $gender, $date_of_birth, $address,
        $contact_number, $nationality, $religion, $bc_number, $bc_division;

    public static function build($array)
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public function __toString()
    {
        return sprintf("%s %s (%s)", $this->first_name, $this->last_name, $this->admission_number);
    }

    /**
     * @param int $id
     * @return Student|null
     */
    public static function find(int $id)
    {
        return Database::find(self::_TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Student[]
     */
    public static function findAll($limit = 1000, $offset = 0)
    {
        return Database::findAll(self::_TABLE, $limit, $offset, self::class, 'admission_no');
    }

    public function insert()
    {
        $data = [
            'admission_number' => $this->admission_number,
            'admission_date' => $this->admission_date,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'gender' => $this->gender,
            'date_of_birth' => $this->date_of_birth,
            'address' => $this->address,
            'contact_number' => $this->contact_number,
            'religion' => $this->religion,
            'nationality' => $this->nationality,
            'bc_number' => $this->bc_number,
            'bc_division' => $this->bc_division,
        ];

        return Database::insert(self::_TABLE, $data);

    }

    public function update(): bool
    {
        $data = [
            'admission_number' => $this->admission_number,
            'admission_date' => $this->admission_date,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'gender' => $this->gender,
            'date_of_birth' => $this->date_of_birth,
            'address' => $this->address,
            'contact_number' => $this->contact_number,
            'religion' => $this->religion,
            'nationality' => $this->nationality,
            'bc_number' => $this->bc_number,
            'bc_division' => $this->bc_division,
        ];

        return Database::update(self::_TABLE, $data, ['id' => $this->id]);

    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }


    /**
     * @param $admission_number
     * @return Student|null
     */
    public static function studentExists($admission_number)
    {
        $db = Database::instance();
        $statement = $db->prepare('SELECT * FROM students WHERE admission_number=?');

        $statement->execute([$admission_number]);

        $result = $statement->fetchObject(self::class);

        if ( !empty($result) ) return $result;
        return null;


    }

    /**
     * @param $query
     * @return Student[]|null
     */
    public static function search($query): ?array
    {

        $db = Database::instance();
        $statement = $db->prepare('SELECT * FROM students WHERE admission_number LIKE :query OR first_name LIKE :query OR last_name LIKE :query LIMIT 30');

        $statement->execute([
            ':query' => '%' . $query . '%'
        ]);

        $result = $statement->fetchAll(PDO::FETCH_CLASS, self::class);
        if ( !empty($result) ) return $result;
        return null;

    }

}