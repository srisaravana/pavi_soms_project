<?php

namespace App\Models;

use App\Core\Database\Database;

class AcademicStaff implements IModel
{

    private const TABLE = 'academic_staffs';

    public ?int $id;
    public ?string $title = "", $fullName = "", $tempAddress = "", $permAddress = "", $email = "", $gender = "", $religion = "", $maritalStatus = "";

    public const TITLE = [
        'MR' => 'Mr.',
        'MRS' => 'Mrs.',
        'MS' => 'Ms.',
    ];

    public const GENDER = [
        'M' => 'Male',
        'F' => 'Female'
    ];

    public const MARITAL_STATUS = [
        'MARRIED' => 'Married',
        'SINGLE' => 'Single',
        'DIVORCED' => 'Divorced',
        'SEPARATED' => 'Separated',
    ];


    public static function build($array)
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return AcademicStaff|null
     */
    public static function find(int $id)
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @return AcademicStaff[]
     */
    public static function findAll($limit = 1000, $offset = 0)
    {
        return Database::findAll(self::TABLE, 1000, 0, self::class, 'fullName');
    }

    /**
     * @return bool|int
     */
    public function insert()
    {
        $data = [
            'title' => $this->title,
            'fullName' => $this->fullName,
            'tempAddress' => $this->tempAddress,
            'permAddress' => $this->permAddress,
            'email' => $this->email,
            'gender' => $this->gender,
            'religion' => $this->religion,
            'maritalStatus' => $this->maritalStatus,
        ];

        return Database::insert(self::TABLE, $data);
    }

    /**
     * @return bool
     */
    public function update()
    {

        $data = [
            'title' => $this->title,
            'fullName' => $this->fullName,
            'tempAddress' => $this->tempAddress,
            'permAddress' => $this->permAddress,
            'email' => $this->email,
            'gender' => $this->gender,
            'religion' => $this->religion,
            'maritalStatus' => $this->maritalStatus,
        ];

        return Database::update(self::TABLE, $data, ['id' => $this->id]);

    }

    /**
     *
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }


    public function getGender()
    {
        return self::GENDER[$this->gender];
    }

    public function getMaritalStatus()
    {
        return self::MARITAL_STATUS[$this->maritalStatus];
    }

}