<?php


namespace App\Models;


use App\Core\Database\Database;

class Subject implements IModel
{

    private const _TABLE = 'subjects';

    public ?int $id;
    public ?string $subject;

    public static function build($array): Subject
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return Subject|null
     */
    public static function find(int $id): ?Subject
    {
        return Database::find(self::_TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Subject[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        return Database::findAll(self::_TABLE, $limit, $offset, self::class, 'subject + 0');
    }

    public function insert()
    {
        $data = [
            'subject' => $this->subject
        ];
        return Database::insert(self::_TABLE, $data);
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            'subject' => $this->subject
        ];
        return Database::update(self::_TABLE, $data, ['id' => $this->id]);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete(self::_TABLE, 'id', $this->id);
    }

    /**
     * @param $subject
     * @return Subject|null
     */
    public static function findBySubject($subject): ?Subject
    {
        $db = Database::instance();
        $statement = $db->prepare('SELECT * FROM ' . self::_TABLE . ' WHERE subject=?');

        $statement->execute([$subject]);

        $result = $statement->fetchObject(self::class);

        if ( !empty($result) ) return $result;
        else return null;

    }
}