<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class Grade implements IModel
{

    private const _TABLE = 'grades';

    public ?int $id, $section_id;
    public ?string $grade_name;


    public static function build($array)
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return Grade|null
     */
    public static function find(int $id)
    {
        return Database::find(self::_TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Grade[]
     */
    public static function findAll($limit = 1000, $offset = 0)
    {
        return Database::findAll(self::_TABLE, $limit, $offset, self::class, 'grade_name + 0');
    }

    public function insert()
    {
        $data = [
            'grade_name' => $this->grade_name,
            'section_id' => $this->section_id
        ];

        return Database::insert(self::_TABLE, $data);

    }

    public function update()
    {
        $data = [
            'grade_name' => $this->grade_name,
            'section_id' => $this->section_id
        ];

        return Database::update(self::_TABLE, $data, ['id' => $this->id]);
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }


    public function getSection()
    {
        return Section::find($this->section_id);
    }

    public static function nameExists($grade_name)
    {
        $db = Database::instance();
        $statement = $db->prepare("SELECT * FROM " . self::_TABLE . " WHERE grade_name = :grade_name");
        $statement->bindValue(':grade_name', $grade_name);
        $statement->execute();

        $result = $statement->fetchObject(Grade::class);

        if ( !empty($result) ) return $result;
        return null;

    }
}