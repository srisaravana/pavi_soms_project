<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class Section implements IModel
{

    private const _TABLE = 'sections';

    public ?int $id, $_order;
    public ?string $section_name;


    public static function build($array): Section
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return Section|null
     */
    public static function find(int $id): ?Section
    {
        return Database::find(self::_TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Section[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        return Database::findAll(self::_TABLE, $limit, $offset, self::class, '_order');
    }

    public function insert()
    {
        $data = [
            'section_name' => $this->section_name,
            '_order' => $this->_order,
        ];

        return Database::insert(self::_TABLE, $data);

    }

    public function update(): bool
    {
        $data = [
            'section_name' => $this->section_name,
            '_order' => $this->_order,
        ];

        return Database::update(self::_TABLE, $data, ['id' => $this->id]);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        return Database::delete(self::_TABLE, 'id', $this->id);
    }


    public static function getAllForDropdown(): array
    {
        $sections = self::findAll();
        $output = [];

        foreach ( $sections as $section ) {
            $output[$section->id] = $section->section_name;
        }

        return $output;
    }

    /**
     * @return Grade[]|null
     */
    public function getAllGrades(): ?array
    {
        $db = Database::instance();
        $statement = $db->prepare("SELECT * FROM grades where section_id=?");
        $statement->execute([$this->id]);
        $result = $statement->fetchAll(PDO::FETCH_CLASS, Grade::class);

        if ( !empty($result) ) return $result;
        return null;

    }

}