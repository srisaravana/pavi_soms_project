<?php


namespace App\Models;


use App\Core\Database\Database;

class SubjectsGroup implements IModel
{

    private const TABLE = 'subjects_groups';

    public ?int $id;
    public ?string $subjects_group_name;


    public static function build($array): SubjectsGroup
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return SubjectsGroup|null
     */
    public static function find(int $id): ?SubjectsGroup
    {
        return Database::find(self::TABLE, $id, self::class);
    }


    /**
     * @param int $limit
     * @param int $offset
     * @return SubjectsGroup[]
     */
    public static function findAll($limit = 1000, $offset = 0): array
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class, 'subjects_group_name');
    }

    /**
     * @return bool|int|null
     */
    public function insert()
    {
        $data = [
            'subjects_group_name' => $this->subjects_group_name,
        ];

        return Database::insert(self::TABLE, $data);
    }


    /**
     * @return bool
     */
    public function update(): bool
    {
        $data = [
            'subjects_group_name' => $this->subjects_group_name,
        ];
        return Database::update(self::TABLE, $data, ['id' => $this->id]);
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $subjectsGroupName
     * @return SubjectsGroup|null
     */
    public static function findBySubjectsGroupName($subjectsGroupName): ?SubjectsGroup
    {
        $db = Database::instance();
        $statement = $db->prepare('SELECT * FROM ' . self::TABLE . ' WHERE subjects_group_name=?');

        $statement->execute([$subjectsGroupName]);

        $result = $statement->fetchObject(self::class);

        if ( !empty($result) ) return $result;
        else return null;
    }

}